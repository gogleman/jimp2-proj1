#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "fileProcessing.h"
#include "generation.h"
#include "graphicspng.h"
#define BUF_LENGTH 128
#define CONFIG_FILE "config"
#define OUTPUT_FILE "outdata"

char buff[BUF_LENGTH], path[128], pomname[24], *name;

int main (int argc, char ** argv){

	//Application initialization
	int boardSize, i;
	char * dataFileName;
	
	Config * conf = prepareConfig();
	readConfig(CONFIG_FILE, conf);
	readParametersFromCommandLine(conf,argc,argv);
	boardSize = getBoardSizeFromConfig(conf);
	Board * b = createBoard(boardSize);
	
	//Read input data
	dataFileName = getDataFileNameFromConfig(conf);
	if (readData(dataFileName, b) != 0){
		printf("ERROR: Problem with readData function.\n");
		return -1;
	}

	//Generate graphics files
	i=0;
	makeFolder("grafika", buff);
	name = pictureName("obraz", i, pomname);
	strcpy( buff,  buildOutPath("grafika", name, path));
	processFile(b);
	writePNGFile(buff);
	cleanBuff(buff);

	for(i=1;i<=getGenerationsNumberFromConfig(conf);i++){
		b=setState(b,conf);
		//printBoard(b);
		//printf("\n");
		if(i%getStepNumberFromConfig(conf)==0){ 

			name = pictureName("obraz", i, pomname);
			strcpy(buff, buildOutPath("grafika", name, path));
			processFile(b);
			writePNGFile(buff);
			cleanBuff(buff);
		}
	}
	printf("INFO: Pliki graficzne zostały wygenerowane: \"results\\grafika\\\"\n");

	//Save output data
	if (saveData(OUTPUT_FILE, b) != 0 ){ 
		return -2;
	}
	printf("INFO: Plik z generacją końcową został wygenerowany: \"%s\"\n",OUTPUT_FILE);

	//End application
	saveConfig(CONFIG_FILE, conf);
	freeBoard(b);
	freeConfig(conf);

	return 0;
}

