#ifndef _WRITEPNG_H
#define _WRITEPNG_H
#include "board.h"

char cleanBuff( char buff[]);

char *pictureName(char* name, int i, char pomname[]);

char *buildOutPath(char *dir, char *name, char path[]);

void makeFolder(char* name, char buff[]);

void writePNGFile(char * file_name);

int blackOrWhite(Board *b, int x, int y);

void processFile(Board *b);

#endif
