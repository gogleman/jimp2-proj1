#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "graphicspng.h"
#include "board.h"
#include "png.h"
#define BUF_LENGTH 128

int x, y, width, height, number_of_passes;
png_byte color_type, bit_depth;
png_structp png_ptr;
png_infop info_ptr;
png_bytep * row_pointers;

char cleanBuff( char buff[]){
	int i;
	for(i=0; i <BUF_LENGTH; i++)
		buff[i]='\0';

	return *buff;
}

char *pictureName(char* name, int i, char pomname[]){
	char pom[10];

	strcpy(pomname, name);
	sprintf(pom, "%d", i);
	strcat(pomname, pom);
	strcat(pomname, ".png");

	return pomname;
}

char *buildOutPath(char *dir, char *name, char path[]){
	strcpy(path, "./results/");
	strcat(path, dir);
	strcat(path, "/");
	strcat(path, name);

	return path;
}

void makeFolder(char* name, char buff[]){
	strcpy(buff, "./results/");
	strcat(buff, name);
	mkdir(buff, 0777);
}

void writePNGFile(char* file_name) {
  FILE *fp = fopen(file_name, "wb");
  if (!fp)
    printf("[write_png_file] File %s could not be opened for writing", file_name);

  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

  if (!png_ptr)
    printf("[write_png_file] png_create_write_struct failed");

  info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr)
    printf("[write_png_file] png_create_info_struct failed");

  if (setjmp(png_jmpbuf(png_ptr)))
    printf("[write_png_file] Error during init_io");

  png_init_io(png_ptr, fp);

  if (setjmp(png_jmpbuf(png_ptr)))
    printf("[write_png_file] Error during writing header");

  png_set_IHDR(png_ptr, info_ptr, width, height,
   bit_depth, color_type, PNG_INTERLACE_NONE,
   PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

  png_write_info(png_ptr, info_ptr);

  if (setjmp(png_jmpbuf(png_ptr)))
    printf("[write_png_file] Error during writing bytes");

  png_write_image(png_ptr, row_pointers);

  if (setjmp(png_jmpbuf(png_ptr)))
    printf("[write_png_file] Error during end of write");

  png_write_end(png_ptr, NULL);

  for (y=0; y<height; y++)
    free(row_pointers[y]);
  free(row_pointers);

  fclose(fp);
}

int blackOrWhite(Board *b, int x, int y){
	int xi=(int)(x/10);
	int yi=(int)(y/10);

	if(b->board[xi][yi] == 1) return 1;
	else return 0;
}

void processFile(Board *b) {
	width = (b->boardSize)*10;
  	height = (b->boardSize)*10;
  	bit_depth = 8;
  	color_type = PNG_COLOR_TYPE_GRAY;
  	number_of_passes = 7;

  	row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
  	for (y=0; y<height; y++)
  		row_pointers[y] = (png_byte*) malloc(sizeof(png_byte) * width);

  	for (y=0; y < height; y++) {
		png_byte* row = row_pointers[y];
		for(x=0; x < width; x++){
			if(blackOrWhite(b, x, y) == 1)
				row[x] = 255;
			else
				row[x] = 0;
		}
	}
}

