#include "board.h"
#include "fileProcessing.h"

#ifndef GENERATION_H
#define GENERATION_H

//Creates the next generation
Board* setState(Board *, Config *);

#endif
