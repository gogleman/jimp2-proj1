#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>	
#include "board.h"
#include "fileProcessing.h"


int readData(char *fileName, Board * board){

	int numberOfCells;
	int linesCounter = 0;
	int cellCoordX,cellCoordY;

	FILE * inFile;
	inFile = fopen(fileName,"r");

	if(inFile == NULL){
		fprintf(stderr,"ERROR: Data file cannot be read\n");
		return -1;
	}
	
	fscanf(inFile, "%d\n", &numberOfCells);

	if(feof(inFile)){
		fprintf(stderr, "ERROR: Data file is empty. Read manual and insert proper values in data file.\n");
		fclose(inFile);
		return -2;
	}
	
	while( !feof(inFile)){
		fscanf(inFile,"%d %d\n",&cellCoordX,&cellCoordY);		
		linesCounter++;
		board->board[cellCoordX][cellCoordY] = 1;
		//printf("Wczytane współrzędne: %d %d\n", cellCoordX, cellCoordY);
	}
	
	fclose(inFile);

	return 0;

}

int saveData(char * fileName, Board * board){

	int numberOfCells = 0;	
	FILE *outFile;
	outFile = fopen(fileName,"w");

	if(outFile == NULL){
		fprintf(stderr,"ERROR: Cannot open file '%s' to save data.\n", fileName);
		return -1;
	}

	for(int i=0; i<board->boardSize;i++){
		for(int j=0;j<board->boardSize;j++){
			if (board->board[i][j] == 1){
				numberOfCells++;
			}
		}
	}

	fprintf(outFile, "%d\n", numberOfCells);	

	for(int i=0; i<board->boardSize;i++){
		for(int j=0;j<board->boardSize;j++){
			if (board->board[j][i] == 1){
				fprintf(outFile, "%d %d\n", j, i);
			}
		}
	}

	fclose(outFile);

	return 0;


}

Config * prepareConfig(){
	Config * newConfig = malloc(sizeof(Config));
	newConfig->dataFileName = malloc(sizeof(char)*30);
	return newConfig;	
}

int readConfig(char * fileName, Config * conf){
	
	int line[10];
	int lineNumber = 0;
	FILE *inFile;
	inFile = fopen(fileName,"r");

	if(inFile == NULL){
		fprintf(stderr,"ERROR: Config file cannot be read\n");
		return -1;
	}

	while(!feof(inFile)){
		fscanf(inFile,"%d\n",&line[lineNumber]);
		lineNumber++;
	}

	conf->boardSize = line[0];
	conf->lastFolderNumber = line[1];
	conf->generationsNumber = line[2];
	conf->stepNumber = line[3];
	conf->setAliveNumber = line[4];
	conf->stayAliveNumber1 = line[5];
	conf->stayAliveNumber2 = line[6];

	fclose(inFile);
	return 0;
}
	


int getBoardSizeFromConfig(Config * conf){
	return conf->boardSize;
}

int getLastFolderNumberFromConfig(Config * conf){
	return conf->lastFolderNumber;
}

int getGenerationsNumberFromConfig(Config * conf){
	return conf->generationsNumber;
}

int getStepNumberFromConfig(Config * conf){
	return conf->stepNumber;
}

int saveConfig(char * fileName, Config * conf){
	
	FILE * outFile;
	outFile = fopen(fileName,"w");

	if(outFile == NULL){
		fprintf(stderr,"ERROR: Cannot open file '%s' to save data.\n", fileName);
		return -1;
	}

	fprintf(outFile, "%d\n",conf->boardSize);
	fprintf(outFile, "%d\n",conf->lastFolderNumber);
	fprintf(outFile, "%d\n",conf->generationsNumber);
	fprintf(outFile, "%d\n",conf->stepNumber);
	fprintf(outFile, "%d\n",conf->setAliveNumber);	
	fprintf(outFile, "%d\n",conf->stayAliveNumber1);
	fprintf(outFile, "%d\n",conf->stayAliveNumber2);

	fclose(outFile);

	return 0;
}

void setBoardSizeToConfig(Config * conf, int boardSize){
	conf->boardSize = boardSize;
}

void setLastFolderNumberToConfig(Config * conf, int lastFolderNumber){
	conf->lastFolderNumber = lastFolderNumber;
}

void setGenerationsNumberToConfig(Config * conf, int generationsNumber){
	conf->generationsNumber = generationsNumber;
}

void setStepNumberToConfig(Config * conf, int stepNumber){
	conf->stepNumber = stepNumber;
}

void freeConfig(Config * conf){
	free(conf->dataFileName);
	free(conf);
}

int readParametersFromCommandLine(Config * conf, int argc, char ** argv){
	
	int bSize = 0;
	int genNumber = 0;
	int stNumber = 0;
	char * dFileName = NULL;
	int c;
	//int opterr = 0;

	while ((c = getopt(argc, argv, "s:g:t:d:")) != -1)
		switch (c){
			case 's':
				bSize = atoi(optarg);
				break;
			case 'g':
				genNumber = atoi(optarg);
				break;
			case 't':
				stNumber = atoi(optarg);
				break;
			case 'd':
				dFileName = optarg;
				break;
			case '?':
				if (optopt == 's')
					fprintf(stderr, "Option -%c requires an argument. Board Size value will be taken from config file.\n", optopt);
				else if (isprint (optopt))
          				fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        			else
          				fprintf (stderr,"Unknown option character `\\x%x'.\n",optopt);
				return 1;
      			default:
        			abort ();
      		}

	if (bSize != 0)
		conf->boardSize = bSize;
	if (genNumber != 0)
		conf->generationsNumber = genNumber;
	if (stNumber != 0)
		conf->stepNumber = stNumber;
	if (dFileName != NULL)
		strcpy(conf->dataFileName, dFileName);
	else strcpy(conf->dataFileName,"data");	

	
	return 0;	
}

char * getDataFileNameFromConfig(Config * conf){

	return conf->dataFileName;
}

