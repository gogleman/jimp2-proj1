#include <stdlib.h>
#include <stdio.h>
#include "board.h"


void setZeros(Board * b){
	for(int i=0; i<b->boardSize;i++){
		for(int j=0;j<b->boardSize;j++){
			b->board[i][j] = 0;
		}
	}
}

void printBoard(Board * b){
	for(int i=0; i<b->boardSize;i++){
		for(int j=0;j<b->boardSize;j++){
			printf("%d | ",b->board[j][i]);
		}
		printf("\n");
	}
}

Board* createBoard(int boardSize){
	Board * newBoard = (Board*)malloc(sizeof(Board));
	
	newBoard->board = (int **)malloc(boardSize*sizeof(int *));
	for(int i=0; i<boardSize;i++)
		newBoard->board[i] = (int*)malloc(boardSize*sizeof(int));
	
	newBoard->boardSize = boardSize;
	setZeros(newBoard);

	return newBoard;
}

void freeBoard(Board * board){
	
	for(int i=0;i<board->boardSize;i++)
		free(board->board[i]);
	free(board->board);
	free(board);

}

