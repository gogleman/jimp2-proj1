#include "board.h"

#ifndef FILEPROCESSING_H
#define FILEPROCESSING_H

//Structure stores configuration data.
typedef struct {
	int boardSize;
	int lastFolderNumber;
	int generationsNumber;
	int stepNumber;
	int setAliveNumber;
	int stayAliveNumber1;
	int stayAliveNumber2;
	char * dataFileName;
	int numberOfConfigParameters;
}Config;

//Reads data from file. Returns 0 on success.
int readData(char *, Board *);

//Saves data to file. Returns 0 on success.
int saveData(char *, Board *);

//Allocates memory for Config structure.
Config * prepareConfig();

//Reads config file. Configuration data is stored in a Config structure. This function is obligatory in order to run 'get' config functions like 'getBoardSizeFromConfig()'.
int readConfig(char * , Config *);

//Returns board size from config file. Value is taken from a Config structure.
int getBoardSizeFromConfig(Config *);

//Returns the number of last folder that was created to store graphic files. Value is taken from a Config structure.
int getLastFolderNumberFromConfig(Config *);

//Returns number of generations to create. Value is taken from a Config structure.
int getGenerationsNumberFromConfig(Config *);

//Returns number of steps describing the number of graphic files to be created. Value is taken from a Config structure.
int getStepNumberFromConfig(Config *);

//Saves configuration data to file. This function must be invoked after running 'set' config functions like 'setBoardSizeToConfig()'
int saveConfig(char *, Config *);

//Sets size of a board. Value is saved in a Config structure.
void setBoardSizeToConfig(Config *, int);

//Sets number of a last folder created. Value is saved in a Config structure.
void setLastFolderNumberToConfig(Config *, int);

//Sets number of generations that are to be created. Value is saved in a Config structure.
void setGenerationsNumberToConfig(Config *, int);

//Sets number of steps describing the number of graphic files to be created. Value is saved in a Config structure.
void setStepNumberToConfig(Config *, int);

//Frees memory of Config structure.
void freeConfig(Config * conf);

//Reads application parameters from command line.
int readParametersFromCommandLine(Config *, int, char **);

//Returns name of a file containing input data. Value is taken from a Config structure.
char * getDataFileNameFromConfig(Config *);

#endif

