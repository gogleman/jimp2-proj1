#include <stdlib.h>
#include <stdio.h>
#include "board.h"
#include "fileProcessing.h"
#include "generation.h"

Board* setState(Board *b, Config * conf){

	int a=0, c=0, k=0, l=0, count=0;
	int setAl = conf->setAliveNumber;
	int stayAl1 = conf->stayAliveNumber1;
	int stayAl2 = conf->stayAliveNumber2;	

	Board * b2 = createBoard(b->boardSize);

	for(int i=1; i<b->boardSize-1;i++){
		for(int j=1;j<b->boardSize-1;j++){
			if(b->board[j][i]==1){
				a=i;
				c=j;
				for(k=a-1;k<=a+1;k++){
					for(l=c-1;l<=c+1;l++){
						if((b->board[l][k]==1) && !((k==a)&&(l==c))){
							count++;
						}
					}
				}
				if(count==stayAl1 || count==stayAl2){
					b2->board[j][i]=1;
				}
			}
			else{
				a=i;
				c=j;
				for(k=a-1;k<=a+1;k++){
					for(l=c-1;l<=c+1;l++){
						if(b->board[l][k]==1){
							count++;
						}
					}
				}
				if (count==setAl){
					b2->board[j][i]=1;
				}
			}
			count=0;
		}
	}
	freeBoard(b);
	b=b2;
	return b;
}

