#ifndef BOARD_H
#define BOARD_H

//Structure storing board with current generation
typedef struct {
	int ** board;
	int boardSize;
} Board;

//Sets all cells of board to zero value
void setZeros(Board *);

//Prints board to stdout 
void printBoard(Board *);

//Creates board. Allocates memory and sets boardSize value
Board* createBoard(int);

//Frees memory of Board structure
void freeBoard(Board *);

#endif
