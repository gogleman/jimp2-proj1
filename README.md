# Conwey's Game of Life 
Conwey's Game of Life is a C program presenting Game of Life cellular automaton

## Functionalities implemented
* Reading from file cells' startup coordinates 
* Generating next state of automaton
* Command line interface
* Generating png files of current state
* Choosing how often (how many steps) the png file must be generated
* Setting number of steps to create
* Saving last state to file


## Technologies used
* C

